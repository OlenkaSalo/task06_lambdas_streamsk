package lambdaExp;
import java.lang.Math;
import java.util.*;

public class Melambda {
    public static void main(String[] args) {
        FuncInterface maxim=(a, b, c) -> {
            int max_1 = Math.max(a,b);
            int max_2 = Math.max(max_1,c);
            return max_2;
        };
          System.out.println(maxim.func(10,15,7));

        FuncInterface avg = (a,b,c)-> ((a+b+c)/3);
          System.out.println(avg.func(3,7,8));
    }
}
