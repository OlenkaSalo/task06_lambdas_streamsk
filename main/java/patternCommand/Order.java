package patternCommand;

public class Order {

    private String foodItem;
    private boolean orderPrep;


    public void takeorder(String order)
    {
        this.foodItem=order;
        System.out.println("You order -" + this.foodItem);
    }

    public void prepered( String orderPlace)
    {
        this.foodItem=orderPlace;
        System.out.println("You order placed");
    }

    public void setOrderPrepered(boolean orderPrep)
    {
        this.orderPrep = orderPrep;
        System.out.println("Custumer order ready!");
    }

    public void serveOrder(String foodItem)
    {
        System.out.println("Your order ready - " + " " + foodItem);
    }


}
