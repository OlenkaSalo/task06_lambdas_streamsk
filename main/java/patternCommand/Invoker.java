package patternCommand;
import java.util.ArrayList;
import java.util.List;

class Waiter {
    private final List<Command> listOrder = new ArrayList();
    public   void addOrder(Command cmdOrder)
    {
        listOrder.add(cmdOrder);
    }

    public  void run()
    {
        listOrder.forEach(Command::execute);
    }
}
