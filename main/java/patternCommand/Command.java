package patternCommand;

public interface Command {
   public void execute();
}
 class takeOrder implements Command{

   private Order order;
   private String str;
   public takeOrder( String str)
   {

      this.str=str;
   }
   @Override
    public void execute() {
      order.takeorder(str);
    }
 }


 class orderPlaced
 {
     private Order order;
     private String string;
     private int count;
     public orderPlaced(String string, int count)
     {

         this.string=string;
         this.count=count;
     }
     Command ordPlace = ()-> {order.prepered(string);};
 }

 class CookOrder implements Command{

    private Order order;

    public CookOrder(Order order)
    {
        this.order = order;
    }

     @Override
     public void execute() {
         order.setOrderPrepered(true);

     }
 }

class servedOrder implements Command{
    private Order order;
    private String string;

    public servedOrder(String string)
    {
        this.string = string;
    }

    @Override
    public void execute() {
        order.serveOrder(string);
    }
}