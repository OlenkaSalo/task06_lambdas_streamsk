package StreamExm;
import java.util.*;
import java.util.stream.Stream;
import java.lang.*;
public class StreamAPI {


    public static void main(String[] args) {


        System.out.println(createRoster());
        System.out.println(reduceSum());


    }

    public static List<Integer> createRoster() {
        Random random = new Random();
        List<Integer> roster = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            int next = random.nextInt(25);
            roster.add(next);
        }
        IntSummaryStatistics funcMath = roster.stream()
                .mapToInt((x) -> x)
                .summaryStatistics();
        Optional<Integer> redSum = roster.stream()
                .reduce((i, j) -> {
                    return i + j;
                });

        System.out.println(funcMath.getMax() + " " + funcMath.getMin() + " " + funcMath.getAverage() + " " + funcMath.getSum());
        System.out.println(redSum);
        return roster;

    }

    public static List<Integer> reduceSum() {
        Random random = new Random();
        Double ave=0.0;
        List<Integer> numberList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            numberList.add(random.nextInt(15));
            ave += ave;
        }
               double  avg = numberList.stream()
                        .mapToInt(x-> x)
                        .average()
                       .getAsDouble();
        System.out.println(avg);
        numberList.stream()
                .filter(p -> p> avg)
                .forEach(System.out::print);

        return numberList;

    }
}



