package Application;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.summingInt;

public class countWord {

    public static void main(String[] args) {
        List<String> wordList =new ArrayList<>();
        Scanner in = new Scanner(System.in);
        String nextLine= null;
        List<String> unique =  Arrays.asList("Olena","math","beatiful");
        while(!(nextLine = in.nextLine()).isEmpty()){

            wordList.add(nextLine);
        }


        long uniqueWords = wordList.stream( )
                .flatMap(line -> Arrays.stream(line.split(" ")))
                .distinct()
                .count();
        List<String> sortedUnwords = wordList.stream()
                .flatMap(i->Arrays.stream(i.split(" ")))
                . distinct()
                .sorted()
                . collect(Collectors.toList())  ;
        Map<String, Integer> collect = wordList.parallelStream()
                .flatMap(i->Arrays.stream(i.split(" ")))
                .collect(Collectors.groupingBy(Function.identity(), summingInt(e -> 1)));
         Map<String, Long> frequentChars = wordList.stream()
                 .flatMap(i->Arrays.stream(i.toLowerCase().split("")))
                 .collect(Collectors.groupingBy(c -> c, Collectors.counting()));

        System.out.println("Number of unique words:"+ uniqueWords);
        System.out.println("Sorted list of all unique words:" + sortedUnwords);
        System.out.println("Word count:"+collect);
        System.out.print(frequentChars);
    }

}
